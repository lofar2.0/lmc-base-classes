# -*- coding: utf-8 -*-
#
# This file is part of the SKA LMC Base Classes project
#
#
#
"""Release information for lmc-base-classes Python Package"""

name = """lmcbaseclasses"""
version = "0.7.0"
version_info = version.split(".")
description = """A set of generic base devices for SKA Telescope."""
author = "SKA India and SARAO and CSIRO"
author_email = "adityadange.ska at gmail.com"
license = """BSD-3-Clause"""
url = """https://www.skatelescope.org/"""
copyright = """NCRA and SARAO and CSIRO"""
